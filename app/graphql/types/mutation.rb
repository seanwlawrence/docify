class Types::Mutation < Types::Base
  field :update_document, mutation: Mutations::UpdateDocument
end
